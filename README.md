# SentimentClassificationUI
Java application for sentiment classification of movie review comments by Naive Bayesian Model!

This is what I do for my master degree of computer science.

To classify the sentiment of movie review into positive or negative, it performs training and classification by Bernoulli Naive Bayes. Before training and classification, certain preprocessing steps are done in follows:
(1) POS tagging using Stanford POS tagger, 
(2) simple negative handling,
(3) feature extraction based on POS (Adj, Adv, Verb, and Noun), and 
(4) feature selection by Information Gain on features using set-of-words model. 
From these gains, some features are selected and ready for Naive Bayes training and classification.